[![coverage report](https://gitlab.com/Belligraf/track-money/badges/master/coverage.svg)](https://gitlab.com/Belligraf/track-money/-/commits/master)
<h1>Track money</h1>
<h3>Vision</h3>
В данном веб приложение вы можете отслеживать свои доходы, а именно распределять доходы на категорий и 
смотреть сколько вы потратили в день/неделю/месяц/год, так же есть история транзакций, где вы сможете посмотреть 
свои расходы 
<h3>Инструкция по запуску:</h3>
Шаг 1: Скачать репозиторий

    git clone (ssh/http key) 

или просто скачайте zip архивом

Шаг 2: Установка виртуально окружение и пакетов
установить виртуально окружение можно командой

    python -m venv venv
и перейдите в него командой

    venv\Scripts\activate
а скачать пакеты командой

    pip install -r requirements.txt
Шаг 3: Настройка базы дынных
в файле local_configs.py на 4 строчке либо измените бд, либо создайте пользователя в постгросе с такими же данными
и установите бд командой 
        
    python init_database.py

Шаг 4: Запуск

    python app.py

from accounts.models import Category, Transaction, User
from database import db

INITIAL_CATEGORIES = ['Без категорий', 'Продукты', 'Транспорт', 'Еда вне дома', 'Развлечения']


def add_users_to_db(email: str, username: str, hashed_password: str):
    user = User(email, username, hashed_password)
    db.session.add(user)
    db.session.commit()

    for category in INITIAL_CATEGORIES:
        add_category(user.id, category)


def add_category(user_id: int, name_category: str):
    category = Category(user_id, name_category)
    db.session.add(category)
    db.session.commit()


def add_transaction(user_id: int, sign: bool, transactions_sum: int, category_id: int):
    transactions = Transaction(user_id, sign, transactions_sum, category_id)
    db.session.add(transactions)
    db.session.commit()

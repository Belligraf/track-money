import datetime

from flask_login import UserMixin
from sqlalchemy import DateTime

from database import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), index=True, unique=True, nullable=False)
    username = db.Column(db.String(64), index=True, unique=False, nullable=False)
    hashed_password = db.Column(db.String(110), unique=False, nullable=False)

    def __init__(self, email: str, username: str, hashed_password: str):
        self.email = email
        self.username = username
        self.hashed_password = hashed_password

    def __repr__(self):
        return f"<User {self.username}>"


class Transaction(db.Model):
    __tablename__ = "transactions"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    date = db.Column(DateTime, default=datetime.datetime.utcnow)
    transactions_sum = db.Column(db.Integer)
    sign = db.Column(db.Boolean)
    category_id = db.Column(db.Integer)

    def __init__(self, user_id: int, sign: bool, transactions_sum: int, category_id: int):
        self.user_id = user_id
        self.sign = sign
        self.transactions_sum = transactions_sum
        self.category_id = category_id


class Category(db.Model):
    __tablename__ = "category"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    name_category = db.Column(db.String(64))

    def __init__(self, user_id, name_category):
        self.user_id = user_id
        self.name_category = name_category

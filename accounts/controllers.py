from flask import request, redirect, url_for, flash, render_template, Blueprint, abort
from flask_login import login_user, login_required, logout_user, current_user

from accounts.services import find_by_username_and_password, NotFoundError, find_user_by_id
from werkzeug.security import generate_password_hash

from accounts.models import User
from accounts.services_for_db import add_users_to_db

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')

POSTS_PER_PAGE = 10


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        remember_me = len(request.form.getlist('remember_me')) > 0
        try:
            user = find_by_username_and_password(email, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=remember_me)
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/login.html')


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('company_management.main_page'))


@accounts_blueprint.route("/user/<int:user_id>", methods=["GET"])
@login_required
def user_info(user_id: int):
    try:
        user = find_user_by_id(user_id)
        return render_template('accounts/user_info.html', user=user)
    except NotFoundError:
        abort(404)


@accounts_blueprint.route("/user", methods=["GET"])
@login_required
def current_user_info():
    return render_template('accounts/user_info.html', user=current_user)


@accounts_blueprint.route('/register', methods=('GET', 'POST'))
def register_user():
    if request.method == 'POST':
        email = request.form.get('email')
        username = request.form.get('username')
        hashed_password = request.form.get('password')
        repeat_hashed_password = request.form.get('repeat_password')
        if not hashed_password:
            flash('Поля ввода пароля обязательно')
            return render_template('accounts/register.html')
        if hashed_password == repeat_hashed_password:
            if User.query.filter_by(email=email).first():
                flash('Пользователь с такой почтой уже существует')
            else:
                add_users_to_db(email, username, generate_password_hash(hashed_password))
                user = find_by_username_and_password(email, hashed_password)
                login_user(user)
                return redirect(url_for('company_management.main_page'))
        else:
            flash('Пароли не совпадают')
    return render_template('accounts/register.html')

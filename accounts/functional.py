from datetime import datetime, timedelta

from flask import render_template, Blueprint, g, request, flash, redirect, url_for
from flask_login import login_required

from accounts.services_for_db import add_transaction, add_category
from database import db

functional_blueprint = Blueprint('functional', __name__, template_folder='templates')


def avg_info_of_balance(sign: bool, start, end):
    return db.engine.execute(
        f"SELECT avg(transactions_sum) as balance FROM transactions WHERE user_id = {g.user.id} and "
        f"sign = {sign} and cast(date as date) between cast('{start}' as date) and cast('{end}' as date);")


@functional_blueprint.route("/statistics", methods=('GET', 'POST'))
@login_required
def statistics():
    today = datetime.now()
    mouth = today.strftime('%Y-%m')
    next_mouth = (today + timedelta(days=31)).strftime('%Y-%m')

    start_mouth = datetime.strptime(mouth + '-01', '%Y-%m-%d')
    end_mouth = datetime.strptime(next_mouth + '-01', '%Y-%m-%d')

    income = avg_info_of_balance(True, start_mouth, end_mouth).first()[0]
    expenses = avg_info_of_balance(False, start_mouth, end_mouth).first()[0]
    return render_template('functional/statistics.html', income=round(income, 2), expenses=round(expenses, 2))


@functional_blueprint.route("/transaction", methods=('GET', 'POST'))
@login_required
def transaction():
    categories = db.engine.execute(f"SELECT id, name_category FROM category WHERE user_id = {g.user.id}")
    if request.method == 'POST':
        try:
            user_sum = int(request.form['sum'])
        except ValueError:
            flash('В сумме содержатся буквы (сумма транзакций должна состоять из цифр)')
        else:
            sign = True
            try:
                if request.form['options-outlined'] == 'minus':
                    user_sum *= -1
                    sign = False
                add_transaction(g.user.id, sign, user_sum, request.form['category_id'])
            except:
                flash('Выберите доход или расход')
                return render_template('functional/transaction.html', categories=categories)
            return redirect(url_for('company_management.main_page'))

    return render_template('functional/transaction.html', categories=categories)


@functional_blueprint.route("/create_category", methods=('GET', 'POST'))
@login_required
def create_category():
    if request.method == 'POST':
        add_category(g.user.id, request.form['category'])
        return redirect(url_for('company_management.main_page'))
    return render_template('functional/create_category.html')


@functional_blueprint.route("/list_category", methods=('GET', 'POST'))
@login_required
def list_category():
    categories = db.engine.execute(f"SELECT name_category FROM category WHERE user_id = {g.user.id}")
    return render_template('functional/list_category.html', categories=categories)


@functional_blueprint.route("/history_transactions", methods=('GET', 'POST'))
@login_required
def history_transactions():
    transactions = db.engine.execute(f"SELECT date, transactions_sum, c.name_category FROM transactions t "
                                     f"INNER JOIN category c on t.category_id = c.id WHERE t.user_id = {g.user.id} "
                                     f"ORDER BY date DESC")
    return render_template('functional/history_transactions.html', transactions=transactions)

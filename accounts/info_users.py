from flask import render_template, Blueprint, g, request, flash, redirect, url_for
from flask_login import login_required
from werkzeug.security import generate_password_hash, check_password_hash

from accounts.controllers import logout
from accounts.models import User
from database import db

user_blueprint = Blueprint('user', __name__, template_folder='templates')


@user_blueprint.route("/info", methods=('GET', 'POST'))
@login_required
def info_myself():
    return render_template('auth/info_user.html', id=g.user.id, username=g.user.username, email=g.user.email)


@user_blueprint.route("/exchange_email", methods=('GET', 'POST'))
@login_required
def exchange_email():
    if request.method == 'POST':
        new_email = request.form.get('new_email')
        db.engine.execute(f"UPDATE users SET email = cast('{new_email}' as text) WHERE id = {g.user.id}")
        return render_template('auth/info_user.html', id=g.user.id, username=g.user.username, email=new_email)
    return render_template('auth/exchange_email.html')


@user_blueprint.route("/exchange_username", methods=('GET', 'POST'))
@login_required
def exchange_username():
    if request.method == 'POST':
        new_username = request.form.get('new_username')
        db.engine.execute(f"UPDATE users SET username = cast('{new_username}' as text) WHERE id = {g.user.id}")
        return render_template('auth/info_user.html', id=g.user.id, username=new_username, email=g.user.email)
    return render_template('auth/exchange_username.html')


@user_blueprint.route("/exchange_password", methods=('GET', 'POST'))
@login_required
def exchange_password():
    if request.method == 'POST':
        old_password = request.form.get('old_password')
        new_password = generate_password_hash(request.form.get('new_password'))
        if check_password_hash(g.user.hashed_password, old_password):
            db.engine.execute(f"UPDATE users SET hashed_password = '{new_password}' WHERE id = {g.user.id}")
            logout()
            return redirect(url_for('company_management.main_page'))
        flash("Неверный старый пароль")
    return render_template('auth/exchange_password.html')

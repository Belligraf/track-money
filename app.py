import os

from flask import Flask, g
from flask_login import current_user
from flask_migrate import Migrate

from accounts.controllers import accounts_blueprint
from accounts.login_manager import login_manager
from accounts.info_users import user_blueprint
from company_management.controllers import company_management_blueprint
from database import db
from error_handlers import page_not_found
from accounts.functional import functional_blueprint
from local_configs import Configuration

app = Flask(__name__)
migrate = Migrate(app, db)


@app.before_request
def before_request():
    g.user = current_user


if not os.getenv('IS_PRODUCTION', None):
    app.config.from_object(Configuration)

db.init_app(app)
login_manager.init_app(app)
app.register_blueprint(accounts_blueprint, url_prefix='/accounts')
app.register_blueprint(company_management_blueprint, url_prefix='/')
app.register_blueprint(functional_blueprint, url_prefix='/functional')
app.register_blueprint(user_blueprint, url_prefix='/user')
app.register_error_handler(404, page_not_found)

if __name__ == '__main__':
    app.run()

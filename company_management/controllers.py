from flask import Blueprint, render_template, g

from company_management.data_processing import data_for_today, data_for_weekday, data_for_mouth, data_for_year

company_management_blueprint = Blueprint('company_management', __name__, template_folder='templates')


@company_management_blueprint.route('/', methods=('GET',))
def main_page():
    data = []
    if g.user.is_authenticated:
        income_for_today, expenses_for_today, balance_for_today = data_for_today()  # this is why i like python
        income_for_weekday, expenses_for_weekday, balance_for_weekday = data_for_weekday()
        income_for_mouth, expenses_for_mouth, balance_for_mouth = data_for_mouth()
        income_for_year, expenses_for_year, balance_for_year = data_for_year()

        data = [
            income_for_today.first()[0],
            income_for_weekday.first()[0],
            income_for_mouth.first()[0],
            income_for_year.first()[0],
            expenses_for_today.first()[0],
            expenses_for_weekday.first()[0],
            expenses_for_mouth.first()[0],
            expenses_for_year.first()[0],
            balance_for_today.first()[0],
            balance_for_weekday.first()[0],
            balance_for_mouth.first()[0],
            balance_for_year.first()[0]
        ]

        for i in range(len(data)):
            if not data[i]:
                data[i] = 0

    return render_template('company_management/main_page.html', data=data)

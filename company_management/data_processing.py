from datetime import datetime, timedelta

from database import db

from flask import g


def info_of_balance(sign: bool, start, end):
    return db.engine.execute(
        f"SELECT sum(transactions_sum) as balance FROM transactions WHERE user_id = {g.user.id} and "
        f"sign = {sign} and cast(date as date) between cast('{start}' as date) and cast('{end}' as date);")


def the_balance(start, end):
    return db.engine.execute(f"SELECT sum(transactions_sum) FROM transactions WHERE user_id = {g.user.id} and "
                             f"cast(date as date) between cast('{start}' as date) and cast('{end}' as date);")


def data_for_today():
    today = datetime.now().strftime('%Y-%m-%d')
    tomorrow = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')

    income_for_today = info_of_balance(True, today, tomorrow)
    expenses_for_today = info_of_balance(False, today, tomorrow)
    balance_for_today = the_balance(today, tomorrow)
    return income_for_today, expenses_for_today, balance_for_today  # it just awesome


def data_for_weekday():
    today = datetime.now()
    days = today.weekday()

    start_weekday = today - timedelta(days=days)
    end_weekday = today + timedelta(days=(7 - days) % 7)

    income_for_weekday = info_of_balance(True, start_weekday, end_weekday)
    expenses_for_weekday = info_of_balance(False, start_weekday, end_weekday)
    balance_for_weekday = the_balance(start_weekday, end_weekday)
    return income_for_weekday, expenses_for_weekday, balance_for_weekday


def data_for_mouth():
    today = datetime.now()
    mouth = today.strftime('%Y-%m')
    next_mouth = (today + timedelta(days=31)).strftime('%Y-%m')

    start_mouth = datetime.strptime(mouth + '-01', '%Y-%m-%d')
    end_mouth = datetime.strptime(next_mouth + '-01', '%Y-%m-%d')

    income_for_mouth = info_of_balance(True, start_mouth, end_mouth)
    expenses_for_mouth = info_of_balance(False, start_mouth, end_mouth)
    balance_for_mouth = the_balance(start_mouth, end_mouth)
    return income_for_mouth, expenses_for_mouth, balance_for_mouth


def data_for_year():
    today = datetime.now()
    year = today.strftime('%Y')
    next_year = (today + timedelta(days=365)).strftime('%Y')

    start_year = datetime.strptime(year + '-01-01', '%Y-%m-%d')
    end_year = datetime.strptime(next_year + '-01-01', '%Y-%m-%d')

    income_for_year = info_of_balance(True, start_year, end_year)
    expenses_for_year = info_of_balance(False, start_year, end_year)
    balance_for_year = the_balance(start_year, end_year)
    return income_for_year, expenses_for_year, balance_for_year

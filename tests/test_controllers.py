from flask import url_for

from accounts.controllers import register_user
from database import db


def test_add_in_db_new_user(client):
    client.post(url_for('accounts.register_user'),
                data=dict(email='email@email', username='username', password='password', repeat_password='password'),
                follow_redirects=True)
    assert db.engine.execute("SELECT username FROM users WHERE email='email@email'").first()[0] == 'username'


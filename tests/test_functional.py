from datetime import datetime, timedelta

from flask import url_for, g

import app as app_module
from accounts.functional import avg_info_of_balance
from accounts.models import Transaction
from database import db

TODAY = datetime.now().strftime('%Y-%m-%d')
TOMORROW = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')


def test_income_avg_info_of_balance(logged_in_client, client):
    app_module.db.session.add(Transaction(1, True, 1234, 1))
    app_module.db.session.add(Transaction(1, True, 123, 2))
    app_module.db.session.add(Transaction(1, False, -123, 3))
    app_module.db.session.add(Transaction(2, True, 123, 1))
    app_module.db.session.commit()
    assert avg_info_of_balance(True, TODAY, TOMORROW).first()[0] == (1234 + 123) / 2


def test_expenses_avg_info_of_balance(logged_in_client, client):
    app_module.db.session.add(Transaction(1, False, -1234, 1))
    app_module.db.session.add(Transaction(1, False, -123, 2))
    app_module.db.session.add(Transaction(1, True, 123, 3))
    app_module.db.session.add(Transaction(2, True, 123, 1))
    app_module.db.session.commit()
    assert avg_info_of_balance(False, TODAY, TOMORROW).first()[0] == (-1234 - 123) / 2


def test_create_category(logged_in_client, client):
    res = client.post(url_for('functional.create_category'), data=dict(category='new_category'))
    assert db.engine.execute(f"SELECT name_category FROM category WHERE user_id = {g.user.id} "
                             f"and name_category='new_category'").first()[0] == 'new_category'



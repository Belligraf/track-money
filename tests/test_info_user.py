from werkzeug.security import check_password_hash

from accounts.login_manager import load_user
from app import db
from flask import url_for
from conftest import logged_in_client


def test_user_search_using_id(logged_in_client, client):
    assert load_user(1).id == 1


def test_exchange_email(logged_in_client, client):
    client.post(url_for('user.exchange_email'), data=dict(new_email='new_mail@mail'), follow_redirects=True)
    assert db.engine.execute('SELECT email FROM users WHERE id=1').first()[0] == 'new_mail@mail'


def test_exchange_username(logged_in_client, client):
    client.post(url_for('user.exchange_username'), data=dict(new_username='new_username'), follow_redirects=True)
    assert db.engine.execute('SELECT username FROM users WHERE id=1').first()[0] == 'new_username'


def test_exchange_password(logged_in_client, client):
    client.post(url_for('user.exchange_password'), data=dict(old_password='123', new_password='new_password'))
    new_password = db.engine.execute('SELECT hashed_password FROM users WHERE id=1').first()[0]
    assert check_password_hash(new_password, 'new_password')

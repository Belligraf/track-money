from datetime import datetime, timedelta

from accounts.models import Transaction
from app import db
from flask import url_for, g
import app as app_module
from company_management.data_processing import info_of_balance, the_balance
from conftest import logged_in_client

TODAY = datetime.now().strftime('%Y-%m-%d')
TOMORROW = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')


def add_transaction(client, data):
    res = client.post(url_for('functional.transaction'), data=data, follow_redirects=True)


def test_add_transaction_in_db(logged_in_client, client):
    data = {'sum': '1234', 'category_id': 1, 'options-outlined': 'plus'}
    res = client.post(url_for('functional.transaction'), data=data)
    assert db.engine.execute("SELECT transactions_sum FROM transactions WHERE transactions_sum=1234 "
                             "and category_id=1").first()[0] == 1234


def test_plus_sum_info_of_balance(logged_in_client, client):
    app_module.db.session.add(Transaction(1, True, 1234, 1))
    app_module.db.session.add(Transaction(1, True, 123, 2))
    app_module.db.session.add(Transaction(1, False, -123, 3))
    app_module.db.session.add(Transaction(2, True, 123, 1))
    app_module.db.session.commit()
    assert info_of_balance(True, TODAY, TOMORROW).first()[0] == 1234 + 123


def test_minus_sum_info_of_balance(logged_in_client, client):
    app_module.db.session.add(Transaction(1, False, -1234, 1))
    app_module.db.session.add(Transaction(1, False, -123, 2))
    app_module.db.session.add(Transaction(1, True, 123, 3))
    app_module.db.session.add(Transaction(2, True, 123, 1))
    app_module.db.session.commit()
    assert info_of_balance(False, TODAY, TOMORROW).first()[0] == -1234 - 123


def test_the_balance(logged_in_client, client):
    app_module.db.session.add(Transaction(1, False, -1234, 1))
    app_module.db.session.add(Transaction(1, False, -123, 2))
    app_module.db.session.add(Transaction(1, True, 123, 3))
    app_module.db.session.add(Transaction(2, True, 123, 1))
    app_module.db.session.commit()
    assert the_balance(TODAY, TOMORROW).first()[0] == -1234

from accounts.services import find_user_by_id, find_by_username_and_password


def test_find_user_by_id(logged_in_client, client):
    assert find_user_by_id(1).email == 'mail@mail'


def test_find_by_username_and_password(logged_in_client, client):
    assert find_by_username_and_password('mail@mail', '123').username == 'Rustem'
